Raytrails Beacon
================

![Raytrails Beacon Map](images/raybeacon-map.png)


On-board Components GPIO Pinout
-------------------------------

Reference	| nRF52833 Pin	| Description
----------------|---------------|------------
SW1		| P1.04		| Tactile push button
SW2		| P0.18		| Tactile push button / hardware RESET
D1		| P0.13		| RGB LED, red
D1		| P0.15		| RGB LED, green
D1		| P0.17		| RGB LED, blue
D2		| P0.22		| Infrared LED


Extension Socket Pinout (2x4 1.27 mm)
-------------------------------------

Board side	| Pin	| Pin	| Edge Side
---------------:|:-----:|:-----:|:------------
P0.04 / AIN2	| 2	| 1	| VDD 1.7-3.6V
P0.06		| 4	| 3	| P0.26
P0.08		| 6	| 5	| P0.12
P1.09		| 8	| 7	| GND


Breadboard Pin Header (1x8 2.54 mm)
-----------------------------------

Pin	| GPIO
--------|-------------
1	| VDD 1.7-3.6V
2	| P0.04 / AIN2
3	| P0.06
4	| P0.08
5	| P0.12
6	| P0.26
7	| P1.09
8	| GND
