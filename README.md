Raytrails DK
============

![Raytrails DK](images/raybeacon-pretty-s.png)

The rayBeacon is full-featured nRF52 based wearable, ultra-low
power, multiprotocol development board designed for variety of
embedded applications. Due to modular design, the device can be
used to build your own production-ready appliance with minimal
hardware modifications.

Key features:

* Coin sized - the board is only 25 mm in diameter
* Works from a single CR2032 / CR2025 3V button cell
* Nordic nRF52 high-end multiprotocol SoC supporting Bluetooth 5.x,
  Bluetooth mesh, Thread and Zigbee; of your choice:
    * [nRF52833](https://www.nordicsemi.com/Products/Low-power-short-range-wireless/nRF52833):
      Cortex-M4F 64MHz, 512KB flash, 128KB RAM,
      Bluetooth® 5.1 Direction Finding, 105°C temperature qualification
    * [nRF52840](https://www.nordicsemi.com/Products/Low-power-short-range-wireless/nRF52840):
      Cortex-M4F 64MHz, 1MB flash, 256KB RAM,
      Bluetooth® 5.0, ARM TrustZone® CryptoCell cryptographic unit
* Onboard ceramic chip antenna, or U.FL connector for demanding apps
* Automotive grade components - ready for harsh environment
* 2 x tactile buttons IP67
* 1 x RGB LED
* 1 x infrared LED (845 nm), can be replaced with any 0402 LED
* Socket for NFC flex antenna, compatible with Nordic FPC antenna
  and Liard 0600-00061. Can be configured as extra 2xGPIO.
* Programmable through SWD port (tear-off Tag-Connect socket)
* 1.27mm pitch 2x4 receptacle to connect
  [custom extension boards](SLICES.md):
    * 6 x GPIO ports
    * 1 x 12-bit ADC input
    * pass-through VDD and GND pins
* 2.54mm pitch 1x8 pin header for fast breadboard prototyping;
  can be reused as 1.27 to 2.54 adapter
* USB interface (on-board solder pads)
* Minimal fabrication cost due to simple two-layers only,
  1.6mm thick board (but see limitations below)
* Open hardware design (BSD license): schematic, PCB, Gerbers, and
  BOM are freely available


Connectors Use Case
-------------------

To match your needs the board has two removable connectors.

On early stages the 1x8 2.54 pin header allows you to connect the
rayBeacon to a breadboard and exeperiment with your own design.
Later, when you won't need it anymore, just breake it off and use
built-in 1.27mm socket to acces the same GPIO ports.

The Tag-Connect 6-pin debug micro-connector offers full control of
the board over SWD protocol and is very handy for regular development.
This is also the connector to load firmware. After the development
stage will be finished the connector can be safely removed thus
leaving the rayBeacon in its most compact shape and ready for
shipping to your customers. Please note, for service needs you can
still connect to the board using test points on the front side.

For more details please also take a look to description of
[onboard components and connectors](MAP.md).


Manufacturing Process
---------------------

Despite the fact the board is built on most advanced nRF52 processors,
it was designed with consideration of keeping your manufacturing bill
low. Such, the board requirements are as follows:

* Dimension: 44 mm x 34 mm
* Layers: 2 layers
* Material: FR-4, TG 130-140
* Thickness: 1.6 mm
* Finished copper thickness: 1 oz/sq.ft
* Min track width / spacing: 5 mil
* Min hole size: 0.3 mm

Also, you may want to drop some components off your BOM in order to
save even more.

[Gerber fabrication files, BOM, and schematic](../../downloads/)
are available for download.


Caveats
-------

Please note the following items when going to manufacture:

* The aQFN73 footprint may require higher PCB production line.
  In particular, it suggests ENIG finish, and requires 5 mil track
  width / clearance for the antenna feed line and the 32.768kHz
  crystal (Y2). On the board both tracks are 0.14 mm (5.51 mils)
  thick. It's recommended to consult your fab for details.
* Please note, the radio wasn't tuned for the nRF52833 yet.
  Use at own risk, or please tune it and report proper values.
