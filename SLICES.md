# Meet Slices

New rayBeacon features can be easily added with an external board
AKA slice attached to the extension socket.

## Orient

![rayBeacon Orient IMU](images/slice-orient-s.png)

The Orient board provides compact, full featured, precise, 9-axis
absolute orientation MEMS solution.  Thanks to the ultra-low power
components it allows maintain drain current at 5 µA in standby, and
below 2.2 mA at full throttle thus extending CR2032 battery life:

* ST LSM303AGR -  high-performance 3D digital linear accelerometer
  and a 3D digital magnetometer
* Bosch BMG250 - low noise, low power three axial gyroscope


## Harvester

![rayBeacon Harvester](images/slice-harvester-s.png)

The Harvester board is designed to provide extended power supply
capabilities to the rayBeacon DK. It can sink from the following
power sources:

* Solar panels (PV modules)
* Thermoelectric generator modules (TEG)
* Micro USB port
* External DC sources, including high capacity battery packs, up to 16V

With additional configuration, the Harvester can expand current
sourcing for the rayBeacon up to 70mA, yet offer a handy USB port
solution.  Thanks to the pass-through extension socket, other
rayBeacon slices can be stacked up easily.

The board employs
[ST SPV1050](https://www.st.com/content/st_com/en/products/power-management/photovoltaic-ics/mppt-dc-dc-converters/spv1050.html)
power harvester with MPPT function in order to avoid collapse of
low-power sources, such as tiny PV panels or TEGs.  The collected
energy is directed to charge
[Maxell ML2032](https://biz.maxell.com/en/rechargeable_batteries/ML2032_DataSheet_16e.pdf)
lithium manganese dioxide battery inserted into the rayBeacon, and
(or) 1500 µF capacitor optionally installed on the Harvester.  It's
also possible to use the Harvester without rechargeable components.

**WARNING!!!  The Harvester board is designed to
[charge only the ML2032](https://biz.maxell.com/en/rechargeable_batteries/ML_17e.pdf)
(or similar) cells.  Li-Ion LIR2032 / LR2032 rechargeable batteries
are not supported! These batteries supply up to 4.2V which is too
high for most of components designed to work from a primary 3V
battery.  To avoid possible damage, do not connect LIR2032 / LR2032
or other Li-Ion and Li-Po cells to the rayBeacon!**

### SPV1050 Configuration

The SPV1050 converter can be configured as boost or buck-boost
DC-DC. Low-power low-voltage sources, like a tiny solar panel
or a TEG, are better work under the boost mode. The Harvester BOM
is tuned to work from a source with Voc<4.7V. Battery end of charge
voltage is set to Veoc=3.1V, and undervoltage protection is set to
Vuvp=2.4V.  The MPPT fixed voltage ratio is set to about 78% with
resistors R2=2.2M and R3=8.06M. For a TEG module with MPP voltage
to Voc ratio is 50%, please use R2=R3=5.2M.

**WARNING!!! In the boost mode input voltage Vin greater than 3.1V
will be passed straight to the battery, the tantalum capacitor, the
rayBeacon, and all attached slices. This may permanently damage
them! Please ensure your input source will never supply voltages
above 3.3V.**

Voltage tolerance values are as follows:

Item					| Rated voltage
----------------------------------------|--------------
ML2032 battery				| 3.3V
nRF52840 / nRF52833 SoC			| 3.6V
Most of components on extension boards	| 3.6V
Tantalum capacitor (TPSE158K004R0075)	| 4.0V

For high voltage sources (2V - 18V) the SPV1050 should be reconfigured
into buck-boost mode. First, adjust the MPPT resistors ladder as
described in the SPV1050 datasheet. Second, please use PCB solder
jumpers on the top layer to configure the board:

![Harvester DCDC Config](images/slice-harvester-cfg.png)

###  Ports and Jumpers

Pad		| PCB Side	| Description
----------------| --------------|------------
VBUS, D+, D-	| front		| USB pass through; to be soldered to the rayBeacon (please note, no GND required)
1V8		| front		| 1.8V SPV1050 LDO output, enabled by the 1.8V LDO jumper
3V2		| front		| 3.2V MIC5205 LDO output, always powered from Micro USB socket
CHG		| back		| Connects (if short) SPV1050 battery charge status pin to rayBeacon socket pin 2 (nRF52 GPIO P0.06)
CONN		| back		| Connects (if short) SPV1050 battery connection status pin to rayBeacon socket pin 4 (nRF GPIO P0.08)
Rlim bypass	| back		| To protect ML2032 battery, current is limited to 2 mA with Rlim resistor. For power demanding apps it's possible to increase the limit up to 70 mA by passing the Rlim; open - 2 mA max, short - 70 mA (**don't forget to remove the battery!**)
1V8 LDO		| back		| Enable (short) or disable (open) internal SPV1050 LDO1 (1.8V), powered by harvesting source or battery
USB charge	| back		| Connects (if short) USB powered 3.2V LDO to SPV1050 store which effectively enables rayBeacon battery charging from USB

Please note, the jumpers cannot be combined in free manner.
Consult Harvester and SPV1050 schematic sheets for details.


### Possible Configuration of PV Panels

For the SPV1050, the rule of thumb is: use *boost* configuration
for a PV panel which open circuit voltage Voc is less than the
secondary battery end of charge voltage Veoc. For every other case,
use the SPV1050 in *buck-boost* mode.

The following table suggests possible configuraion of PV panels
with total area constraint set to 25x25 mm. All panels connected
in series.

Panel | Voc, V | Isc, A | Pmax, W | Size, mm | Boost Panels | Buck-Boost Panels
----- | ------ | ------ | ------- | -------- | ------------ | -----------------
Amorton [AM-1456](https://panasonic.co.jp/ls/psam/en/products/pdf/Catalog_Amorton_ENG.pdf), indoor | 2.4 | 6µ | 7.95µ | 25x10 | 1 | 2
Amorton [AT-26L0B](https://panasonic.co.jp/ls/psam/en/products/pdf/Catalog_Amorton_ENG.pdf), indoor | | | 38µ | 26 (round) | 1 | -
Amorton [AM-5610](https://panasonic.co.jp/ls/psam/en/products/pdf/Catalog_Amorton_ENG.pdf) | 5.1 | 2.4m | 17.9m | 25x20 | - | 1
IXYS [KXOB25-14X1F](https://ixapps.ixys.com/PartDetails.aspx?pid=14659&r=1) | 0.69 | 58.6m | 30.8m | 23x8 | 3 | -
IXYS [KXOB25-05X3F](https://ixapps.ixys.com/PartDetails.aspx?pid=14658&r=1) | 2.07 | 19.5m | 30.7m | 23x8 | 1 | 3
IXYS [KXOB25-02X8F](https://ixapps.ixys.com/PartDetails.aspx?pid=14657&r=1) | 5.53 | 6.3m | 26.3m | 23x8 | - | up to 3


## Manufacturing Process

Capability		| Orient, Harvester
------------------------|------------------
Dimension		| Ø25 mm
Material		| FR-4
Thickness		| 1.6 mm
Finished copper 	| 1 oz/sq.ft
Layers			| 2 layers
Min track / spacing	| 6 mil	
Min via hole size	| 0.4 mm
